import express from 'express';
import User from '../models/user.js';

const router = express.Router();

router.post('/api/register', async (req, res) => {
  const fingerprint = await User.findOne({ fingerprint: req.body.fingerprint }).exec();

  if (!!fingerprint && fingerprint !== req.body.fingerprint) {
    return res.status(400).send({ error: 'Error: You have already created an account!' })
  }

  const exists = await User.findOne({ email: req.body.email }).exec();
 
  if (exists) {
    return res.status(400).send({ error: 'Error: User with this email already exists.' });
  }

  const balance = Math.floor(Math.random() * (100000000 - 1000000 + 1) + 1000000);
  const newUser = new User({ ...req.body, balance });

  try { 
    await newUser.save();
  } catch (e) {
    console.log(e);
    return res.status(400).send({ error: e });
  }

  res.send(newUser);
});

router.post('/api/login', async (req, res) => {
  const user = await User.findOne({ email: req.body.email, password: req.body.password }).exec();
  
  if (!user) {
    return res.status(400).send({ error: 'Error: Invalid username/and or password.' });
  }

  if (user.fingerprint !== req.body.fingerprint) {
    return res.status(400).send({ error: 'Error: You need to confirm your identity. A confirmation email has been sent.' })
  }

  const { first_name, last_name, email, fingerprint, balance } = user;

  res.send({ first_name, last_name, email, fingerprint, balance });
});

export default router;
