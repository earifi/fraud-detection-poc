import mongoose from 'mongoose';
import express from 'express';
import cors from 'cors';
import userRouter from './routes/user.js'

try {
  await mongoose.connect('mongodb://localhost:27017/fraud-detection', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  console.log('Connected to MongoDB.');
} catch (e) {
  console.error(e);
}

const app = express();

app.use(cors())
app.use(express.json());
app.use(userRouter);

app.listen(3001, () => {
  console.log('Listetning on port 3001.');
});
