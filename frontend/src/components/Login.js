import axios from 'axios';
import React, { useState } from 'react';
import { Form, Row, Col, Button, Alert } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

function Login({ visitorId, location }) {
  const [success, setSuccess] = useState(null);
  const [error, setError] = useState('');

  const [form, setForm] = useState({
    email: '',
    password: '',
  });

  const submitForm = async (event) => {
    event.preventDefault();

    try {
      const { data } = await axios.post('http://localhost:3001/api/login', {
        ...form,
        fingerprint: visitorId,
      });
      localStorage.setItem('user', JSON.stringify(data));

      setSuccess(true);
    } catch (e) {
      console.log(e);
      setError(e.response.data.error);
    }
  };

  const onFormChange = (event) => {
    const { name, value } = event.target;

    setForm((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  };

  if (success) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <div className="offset-md-3 col-md-6">
      {location.reigistrationSuccessful && (
        <Alert variant="success" className="mt-4">
          Your registration was successful!
        </Alert>
      )}
      <h2 className="mt-4 mb-4">Login</h2>
      {error && <Alert variant="danger">{error}</Alert>}
      <Form onSubmit={submitForm}>
        <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Email
          </Form.Label>
          <Col sm={10}>
            <Form.Control
              type="email"
              name="email"
              required
              placeholder="Email"
              value={form.email}
              onChange={onFormChange}
            />
          </Col>
        </Form.Group>

        <Form.Group
          as={Row}
          className="mb-3"
          controlId="formHorizontalPassword"
        >
          <Form.Label column sm={2}>
            Password
          </Form.Label>
          <Col sm={10}>
            <Form.Control
              type="password"
              name="password"
              required
              placeholder="Password"
              value={form.password}
              onChange={onFormChange}
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} className="mb-3">
          <Col sm={{ span: 10, offset: 2 }}>
            <Button type="submit">Login</Button>
          </Col>
        </Form.Group>
      </Form>
    </div>
  );
}

export default Login;
