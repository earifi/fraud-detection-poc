import React, { useState } from 'react';
import { Form, Row, Col, Button, Alert } from 'react-bootstrap';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

function Register({ visitorId }) {
  const [error, setError] = useState('');
  const [success, setSuccess] = useState(null);

  const [form, setForm] = useState({
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    confirm_password: '',
  });

  const submitForm = async (event) => {
    event.preventDefault();

    if (form.password !== form.confirm_password) {
      setError('Passwords do not match.');
      return;
    }

    try {
      await axios.post('http://localhost:3001/api/register', {
        ...form,
        fingerprint: visitorId,
      });

      setSuccess(true);
    } catch (e) {
      setError(e.response.data.error);
    }
  };

  const onFormChange = (event) => {
    const { name, value } = event.target;

    setForm((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  };

  if (success) {
    return (
      <Redirect to={{ pathname: '/login', reigistrationSuccessful: true }} />
    );
  }

  return (
    <div className="offset-md-3 col-md-6">
      <div className="row">
        {error && (
          <Alert variant="danger" className="mt-4">
            {error}
          </Alert>
        )}
        <h2 className="mt-4 mb-4">Register</h2>
        <Form onSubmit={submitForm}>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Form.Label column sm={2}>
              First name
            </Form.Label>
            <Col sm={10}>
              <Form.Control
                type="text"
                placeholder="First name"
                name="first_name"
                required
                value={form.first_name}
                onChange={onFormChange}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Form.Label column sm={2}>
              Last name
            </Form.Label>
            <Col sm={10}>
              <Form.Control
                type="text"
                placeholder="Last name"
                name="last_name"
                required
                value={form.last_name}
                onChange={onFormChange}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Form.Label column sm={2}>
              Email
            </Form.Label>
            <Col sm={10}>
              <Form.Control
                type="email"
                placeholder="Email"
                name="email"
                required
                value={form.email}
                onChange={onFormChange}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formHorizontalPassword"
          >
            <Form.Label column sm={2}>
              Password
            </Form.Label>
            <Col sm={10}>
              <Form.Control
                type="password"
                placeholder="Password"
                name="password"
                required
                value={form.password}
                onChange={onFormChange}
              />
            </Col>
          </Form.Group>
          <Form.Group
            as={Row}
            className="mb-3"
            controlId="formHorizontalPassword"
          >
            <Form.Label column sm={2}>
              Confirm Password
            </Form.Label>
            <Col sm={10}>
              <Form.Control
                type="password"
                placeholder="Password"
                name="confirm_password"
                required
                value={form.confirm_password}
                onChange={onFormChange}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row} className="mb-3">
            <Col sm={{ span: 10, offset: 2 }}>
              <Button type="submit">Register</Button>
            </Col>
          </Form.Group>
        </Form>
      </div>
    </div>
  );
}

export default Register;
