import React, { useState, useEffect } from 'react';

const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

function Dashboard() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    setUser(JSON.parse(localStorage.getItem('user')));
  }, []);

  if (user === null) {
    return <h1>Loading...</h1>;
  }

  return (
    <div>
      <h1>
        Welcome back, {user.first_name} {user.last_name}!
      </h1>
      <h3>Balance: {formatter.format(user.balance)} USD</h3>
      <p>Your unique fingerprint is: {user.fingerprint} </p>
    </div>
  );
}

export default Dashboard;
