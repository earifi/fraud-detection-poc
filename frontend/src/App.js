import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Navbar, Container, Nav } from 'react-bootstrap';
import FingerprintJS from '@fingerprintjs/fingerprintjs';
import { Register, Login, Home, Dashboard } from './components';

function App() {
  const [visitorId, setVisitorId] = useState(null);

  useEffect(() => {
    (async () => {
      const agent = await FingerprintJS.load();
      const fingerprint = await agent.get();
      setVisitorId(fingerprint.visitorId);
      console.log(fingerprint.visitorId);
    })();
  }, []);

  return (
    <Router>
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="#home">Super Secure Bank</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse
            id="basic-navbar-nav"
            className="justify-content-end"
          >
            <Nav className="ml-auto">
              <Nav.Link as={Link} to="/login">
                Login
              </Nav.Link>
              <Nav.Link as={Link} to="/register">
                Register
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Container>
        <Switch>
          <Route
            exact
            path="/login"
            render={(props) => <Login {...props} visitorId={visitorId} />}
          />
          <Route
            exact
            path="/dashboard"
            render={(props) => <Dashboard {...props} visitorId={visitorId} />}
          />
          <Route
            exact
            path="/register"
            render={(props) => <Register {...props} visitorId={visitorId} />}
          />
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
