const puppeteer = require('puppeteer');
const faker = require('faker');

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();

  for (let i = 0; i < 10; i++) {
    await page.goto('http://localhost:3000/register');

    const firstName = faker.name.firstName();
    const lastName = faker.name.firstName();
    const domain = faker.internet.domainName();
    const email = `${firstName}.${lastName}@${domain}`.toLowerCase();
    const password = faker.internet.password();

    await page.type('input[name="first_name"]', firstName, {delay: 5});
    await page.type('input[name="last_name"]', lastName, {delay: 5});
    await page.type('input[name="email"]', email, {delay: 5});
    await page.type('input[name="password"]', password, {delay: 5});
    await page.type('input[name="confirm_password"]', password, {delay: 5});
    await page.click('button[type="submit"]', {delay: 5});
    await page.waitForNetworkIdle();
  }

  await browser.close();
})();
