const puppeteer = require('puppeteer');
const list = require('./list.json');

(async () => {
  let i;
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  browser.on('targetchanged', (target) => {
    const url = target.url();
    if (url === 'http://localhost:3000/dashboard') {
      console.log(`Login successful: ${list[i]}`);
    }
  });

  for (i = 0; i < list.length; i++) {
    await page.goto('http://localhost:3000/login');

    const [email, password] = list[i].split(':');

    await page.type('input[name="email"]', email, {delay: 5});
    await page.type('input[name="password"]', password, {delay: 5});
    await page.click('button[type="submit"]', {delay: 5});
    await page.waitForNetworkIdle();

    try {
      let element = await page.$('.alert')
      let value = await page.evaluate(el => el.textContent, element)

      console.log(`Login failed: ${list[i]}, Error: ${value}`);
    } catch (e) {
    }
  }

  await browser.close();
})();
